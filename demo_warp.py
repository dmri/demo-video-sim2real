import torch
from PIL import Image
import cv2
from torchvision.transforms.functional import to_tensor
from torchvision.utils import save_image

import util

if __name__ == '__main__':

	# images
	# shape: 3 x H x W
	translation_src = to_tensor(Image.open('data/translation0032.png'))
	translation_tgt = to_tensor(Image.open('data/translation0041.png'))
	
	# pixel-wise 3D coordinates
	# shape: H x W x 3
	points3D_src = torch.Tensor(cv2.imread('data/coords0032.exr', cv2.IMREAD_UNCHANGED))
	points3D_tgt = torch.Tensor(cv2.imread('data/coords0041.exr', cv2.IMREAD_UNCHANGED))
	
	# camera pose
	# shape: 7 x 1 (3 location coordinates + 4 quaternion rotation coordinates (concatenated))
	cam_pose_src = torch.Tensor(cv2.imread('data/pose0032.exr', cv2.IMREAD_UNCHANGED))
	cam_pose_tgt = torch.Tensor(cv2.imread('data/pose0041.exr', cv2.IMREAD_UNCHANGED))

	# get projection matrix
	_,_,projection_matrix_src = util.get_camera_matrices(cam_pose_src,points3D_src)
	_,_,projection_matrix_tgt = util.get_camera_matrices(cam_pose_tgt,points3D_tgt)

	# add a batch dimension to tensors and load to gpu (bc the functions were written for usage during training)
	translation_src = translation_src.unsqueeze(dim=0).cuda()
	translation_tgt = translation_tgt.unsqueeze(dim=0).cuda()
	points3D_src = points3D_src.unsqueeze(dim=0).cuda()
	points3D_tgt = points3D_tgt.unsqueeze(dim=0).cuda()
	projection_matrix_src = projection_matrix_src.unsqueeze(dim=0).cuda()
	projection_matrix_tgt = projection_matrix_tgt.unsqueeze(dim=0).cuda()



	#----------------------------------------------------------------#
	#-- WARP PIXELS (COLOR VALUES) INTO OTHER VIEWS (CAMERA POSES) --#
	#----------------------------------------------------------------#

	# sanity check: reproject pixels into own view (should result in identity)
	warped_same_view, _ = util.warp(projection_matrix_src,points3D_src,translation_src)
	save_image(warped_same_view,'warped_same_view.png')
	print('- Saved image that was reprojected into its own view as "warped_same_view.png"')

	# warp pixels of translation_1 into view of translation_2:
	warped_into_tgt, z_warped_src = util.warp(projection_matrix_tgt,points3D_src,translation_src)
	save_image(warped_into_tgt,'warped_before_occl_removal.png')
	print('- "translation_src" warped into view of "translation_tgt", saved as "warped_before_occl_removal.png". However, occluded areas are incorrectly rendered.')
	# but note that this doesn't handle occlusions, so:
	warped_into_tgt = util.remove_occlusions(warped_into_tgt,z_warped_src,projection_matrix_tgt,points3D_tgt)
	# note: 'z_warped_src' are the z-coordinates of the 3D coordinates in camera space. hence, z is proportional to the depth and can be used to find occlusions.
	# i.e. z_warped_src contains the distances of the SOURCE image's 3D locations to the TARGET camera view.
	# 'projection_matrix_tgt' and 'points3D_tgt' are used to obtain the pixel-wise depth in the target image for comparison.
	save_image(warped_into_tgt,'warped_into_tgt.png')
	print('- Saved "warped_into_tgt.png" after removing occlusions.')