import torch
from PIL import Image
import cv2
from torchvision.transforms.functional import to_tensor
from mathutils import Vector, Quaternion

if __name__ == '__main__':

	#---------------#
	#-- LOAD DATA --#
	#---------------#

	# input
	# shape: 3 x H x W
	input_img = to_tensor(Image.open('data/input0032.png'))
	print('input (reference image) shape: {}'.format(input_img.shape))

	# images
	# shape: 3 x H x W
	translation = to_tensor(Image.open('data/translation0032.png'))
	print('output (translation) shape: {}'.format(translation.shape))

	# segmentation maps
	# shape: 3 x H x W
	seg = to_tensor(Image.open('data/segmentation0032.png'))
	print('segmentation shape: {}'.format(seg.shape))

	# depth maps
	# shape: H x W
	depth = torch.Tensor(cv2.imread('data/depth0032.exr', cv2.IMREAD_UNCHANGED))
	print('depth shape: {}'.format(depth.shape))
	
	# pixel-wise 3D coordinates
	# shape: H x W x 3
	points3D = torch.Tensor(cv2.imread('data/coords0032.exr', cv2.IMREAD_UNCHANGED))
	print('3D-cooridnates shape: {}'.format(points3D.shape))
	
	# camera pose
	# shape: 7 x 1 (3 location coordinates + 4 quaternion rotation coordinates (concatenated))
	cam_pose = torch.Tensor(cv2.imread('data/pose0032.exr', cv2.IMREAD_UNCHANGED))
	print('camera pose shape: {}'.format(cam_pose.shape))
	cam_pose = cam_pose.squeeze(axis=-1)
	location, rotation = cam_pose[:3], cam_pose[3:]
	location = Vector(location)
	rotation = Quaternion(rotation)