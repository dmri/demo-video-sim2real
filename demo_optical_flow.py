import torch
from PIL import Image
import cv2
from torchvision.transforms.functional import to_tensor
import numpy as np

import util

if __name__ == '__main__':


	#---------------#
	#-- LOAD DATA --#
	#---------------#
	
	# pixel-wise 3D coordinates of 2 consecutive frames
	# shape: H x W x 3
	points3D_1 = torch.Tensor(cv2.imread('data/coords0010.exr', cv2.IMREAD_UNCHANGED))
	points3D_2 = torch.Tensor(cv2.imread('data/coords0011.exr', cv2.IMREAD_UNCHANGED)) # not really required for OF computation. only to get the projection matrix.
	
	# camera poses
	# shape: 7 x 1 (3 location coordinates + 4 quaternion rotation coordinates (concatenated))
	cam_pose_1 = torch.Tensor(cv2.imread('data/pose0010.exr', cv2.IMREAD_UNCHANGED))
	cam_pose_2 = torch.Tensor(cv2.imread('data/pose0011.exr', cv2.IMREAD_UNCHANGED))

	# get projection matrices for both views
	_, _, projection_matrix_1 = util.get_camera_matrices(cam_pose_1,points3D_1)
	_, _, projection_matrix_2 = util.get_camera_matrices(cam_pose_2,points3D_2)


	#-------------------------------#
	#-- GROUND-TRUTH OPTICAL FLOW --#
	#-------------------------------#

	# compute optical flow
	# represented as pixel-wise (delta-x,delta-y) pairs
	# shape: H x W x 2
	optical_flow_gt = util.get_gt_optical_flow(points3D_1,projection_matrix_1,projection_matrix_2)


	#--------------------------------------------------------------#
	#-- ESTIMATED OPTICAL FLOW (GUNNAR-FARNEBACK) FOR COMPARISON --#
	#--------------------------------------------------------------#

	# load gray-scale images
	translation_1 = cv2.imread('data/translation0010.png',cv2.IMREAD_GRAYSCALE)
	translation_2 = cv2.imread('data/translation0011.png',cv2.IMREAD_GRAYSCALE)
	# estimate optical flow
	optical_flow_est = cv2.calcOpticalFlowFarneback(translation_1, translation_2, None, 0.5, 3, 15, 3, 5, 1.2, 0)


	#----------------------------------#
	#-- VISUALIZE FLOW AS HSV IMAGES --#
	#----------------------------------#
	# from: https://www.geeksforgeeks.org/opencv-the-gunnar-farneback-optical-flow/

	# Create mask
	H, W = translation_1.shape[0], translation_1.shape[1]
	hsv_mask = np.zeros((H,W,3))
	# Make image saturation to a maximum value
	hsv_mask[..., 1] = 255
	
	### ground-truth flow
	optical_flow_gt = optical_flow_gt.numpy()
	# convert to HSV and save
	mag, ang = cv2.cartToPolar(optical_flow_gt[..., 0], optical_flow_gt[..., 1])
	hsv_mask[..., 0] = ang * 180 / np.pi / 2
	hsv_mask[..., 2] = cv2.normalize(mag, None, 0, 255, cv2.NORM_MINMAX)
	hsv_mask = np.uint8(hsv_mask)
	rgb_representation = cv2.cvtColor(hsv_mask, cv2.COLOR_HSV2BGR)
	cv2.imwrite('ground_truth_optical_flow.png', rgb_representation)
	
	### estimated flow
	# convert to HSV and save
	mag, ang = cv2.cartToPolar(optical_flow_est[..., 0], optical_flow_est[..., 1])
	hsv_mask[..., 0] = ang * 180 / np.pi / 2
	hsv_mask[..., 2] = cv2.normalize(mag, None, 0, 255, cv2.NORM_MINMAX)
	hsv_mask = np.uint8(hsv_mask)
	rgb_representation = cv2.cvtColor(hsv_mask, cv2.COLOR_HSV2BGR)
	cv2.imwrite('estimated_optical_flow.png', rgb_representation)